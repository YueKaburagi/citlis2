
module Citlis.Core where

import Prelude hiding (and, zip, log)

import Data.Semigroup ((<>))
import Data.Key (zip, replace)
import Data.Bool (bool)
import Data.Coerce (coerce)
import Data.Maybe (fromMaybe)
import qualified Data.List as List
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Foldable (toList)
import Data.Function ((&))
import Data.Bifunctor (first, second, bimap)
import Data.CircularList (CList)
import qualified Data.CircularList as CList
import Data.Random (shuffle, runRVar, StdRandom(StdRandom))
import Data.Ratio ((%))
import Data.Default.Class
import Control.Concurrent (newMVar, newEmptyMVar, forkIO, killThread, MVar, readMVar, tryTakeMVar, modifyMVar_, threadDelay, yield)
import Control.Lens (view, preview, set, over, _Just, coerced, modifying, assign, ix)
import Control.Monad (void, join)
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.State.Lazy (StateT)
import qualified Control.Monad.Trans.State.Lazy as State

import Citlis.Types
import Citlis.View.Types (UserAction)
import qualified Citlis.View.Types as V
import Citlis.Field
import Citlis.Mino (minoset, dummyMino, rotateR, rotateL, resetRotation, shiftGhost)
import Citlis.Mino.Types (Mino(..))
import qualified Citlis.Mino.Types as Mino




nextSeven :: IO [Mino]
nextSeven = runRVar (shuffle minoset) StdRandom

coreOneStep :: MVar PortC -> MVar PortI -> CitlisCoreLoop ()
coreOneStep portC portI = do
  input <- lift $ tryTakeMVar portI
  maybe (pure ()) (\(PortI v) -> applyUA v) input
  landingSection portC
  spawnSection
  gravitySection
  dt <- State.get
  lift $ modifyMVar_ portC (pure . fCDtPC dt)
  where
    applyUA :: UserAction -> CitlisCoreLoop ()
    applyUA V.Quit = lift $ modifyMVar_ portC (pure . set _systemData True)
    applyUA V.SoftDrop = applyLoop SoftDrop portC
    applyUA V.MoveR = applyLoop MoveRight portC
    applyUA V.MoveL = applyLoop MoveLeft portC
    applyUA V.HardDrop = applyLoop HardDrop portC
    applyUA V.RotateR = applyLoop RotateR portC
    applyUA V.RotateL = applyLoop RotateL portC
    applyUA V.Hold = applyLoop Hold portC

coreLoop :: MVar PortC -> MVar PortI -> CitlisCoreLoop ()
coreLoop portC portI = do
  coreOneStep portC portI
  st <- lift $ readMVar portC
  if view _systemData st
    then pure ()
    else lift (threadDelay 16666) >> coreLoop portC portI


landingSection :: MVar PortC -> CitlisCoreLoop ()
landingSection port = do
  cd <- State.get
  bool (pure ()) (
    case (+ negate 16) . coerce <$> Map.lookup LockTimer (view _timers cd) of
      Just msec
        | msec < 0 -> doLock port
        | otherwise -> assign (_timers . ix LockTimer) (Millisec msec)
      Nothing -> modifying _timers (Map.insert LockTimer $ genLockTimer def)
    ) (isL cd)
  where
    mino cd = CList.focus =<< preview (_minoState . _Just . _minoRing) cd
    pos = preview (_minoState . _Just . _pos)
    isL cd = fromMaybe False $ isLanding <$> mino cd <*> pos cd <*> preview _field cd
    genLockTimer (Configure timers _) = fromMaybe (Millisec 0) $ Map.lookup LockDelay timers

doLock :: MVar PortC -> CitlisCoreLoop ()
doLock port = do
  cd <- State.get
  modifying _timers dropTimer
  let lastMinoState = minostate cd
  let lastField = view _field cd
  case setMino <$> mino cd <*> pos cd <*> field cd of
    Just nf -> assign _field nf
    Nothing -> pure ()
  assign _minoState Nothing
  modifying _timers (Map.insert SpawnTimer $ genSpawnTimer def)
  lift $ pushPhenomenon (genLockEffect cd) port
  maybe (pure ()) (clearLineSection port) lastMinoState
  gameoverSection port lastField
  where
    dropTimer = Map.delete LockTimer
    genSpawnTimer (Configure timers _) = fromMaybe (Millisec 0) $ Map.lookup SpawnDelay timers
    mino cd = CList.focus =<< preview (_minoState . _Just . _minoRing) cd
    pos = preview (_minoState . _Just . _pos)
    field = preview _field
    minostate = preview (_minoState . _Just)

pushPhenomenon :: Phenomenon -> MVar PortC -> IO ()
pushPhenomenon phn port =
  modifyMVar_ port ( pure . over (_gfxData . _latestPhenomena) (pushP phn))
  where
    pushP p l = (p, hindex l) : l
    hindex l = maybe 0 ((+1) . snd) $ fst <$> List.uncons l
takeSnapshot :: CoreData -> ViewField
takeSnapshot = takeViewField

-- current Mino はこれが呼ばれている時点で失われているはず(lockの操作で消失)なので貰わないといけない
clearLineSection :: MVar PortC -> MinoState -> CitlisCoreLoop ()
clearLineSection port (MinoState cm pos log _) = do
  cd <- State.get
  let fld = view _field cd
  let (nf,l) = clearLine fld
  if null l
    then pure ()
    else do
      lift $ pushPhenomenon (genClearLine l cd) port
      case genErasion l fld =<< CList.focus cm of
        Just p -> lift $ pushPhenomenon p port
        Nothing -> pure ()
      assign _field nf
      modifying (_timers . ix SpawnTimer . coerced) (+ genEraceDelay def)
  where
    genClearLine l cd = PClearLine (takeSnapshot cd) l
    genErasionEazy l (Mino c _ _ _) =
      case length l of
        4 -> pure $ PErasion ETetris c
        3 -> pure $ PErasion ETriple c
        2 -> pure $ PErasion EDouble c
        1 -> pure $ PErasion ESingle c
        _ -> Nothing
    genErasion l fld mino@(Mino c _ _ _)
      | isSuperRotationLast log && length l == 1 = pure $ PErasion ESpinMini c
      | isSpinInLocation mino pos fld =
        if isRotationLast log
          then
            case length l of
              1 -> pure $ PErasion ESpinSingle c
              2 -> pure $ PErasion ESpinDouble c
              3 -> pure $ PErasion ESpinTriple c
              _ -> Nothing
          else genErasionEazy l mino
      | otherwise = genErasionEazy l mino
    genEraceDelay :: Configure -> Int
    genEraceDelay (Configure timers _) = maybe 0 coerce $ Map.lookup ErasionDelay timers


isRotationLast :: [MinoAction] -> Bool
isRotationLast [] = False
isRotationLast (Lock:xs) = isRotationLast xs
isRotationLast (x:_) =
  x `elem` [RotateR, RotateL, SuperRotateR, SuperRotateL]
isSuperRotationLast :: [MinoAction] -> Bool
isSuperRotationLast [] = False
isSuperRotationLast (Lock:xs) = isRotationLast xs
isSuperRotationLast (x:_) =
  x `elem` [SuperRotateR, SuperRotateL]


genLockEffect :: CoreData -> Phenomenon
genLockEffect cd =
  PLockMino
  (takeSnapshot cd)
  ((+ negate 20) <$> minoToCellNums
    (fromMaybe dummyMino (CList.focus =<< preview (_minoState . _Just . _minoRing) cd))
    (fromMaybe (0,0) (preview (_minoState . _Just . _pos) cd))
    (view _field cd)
  )


gameoverSection :: MVar PortC -> Field -> CitlisCoreLoop ()
gameoverSection port beforeField = do
  cd <- State.get  
  currentView <- takeSnapshot <$> State.get
  if fieldToView beforeField == currentView || not (canSpwan (lookAheadMino $ view _nextMinos cd) (preview _field cd))
    then do
    assign _timers mempty
    lift $ pushPhenomenon (PGameOver currentView) port
    else
    pure ()
  where
    canSpwan (a,p) field = (== Just True) $ isValidPos <$> CList.focus a <*> pure p <*> field


doSpawn :: CitlisCoreLoop ()
doSpawn = do
  modifying _timers dropTimer
  nexts <- view _nextMinos <$> State.get
  modifying id (conn $ spawnMino nexts)
  stateModifyM supplyNexts
  where
    conn (minoRing, pos, rest) =
      set _minoState (Just (MinoState minoRing pos mempty False)) .
      set _nextMinos rest
    dropTimer = Map.delete SpawnTimer
    stateModifyM f = State.get >>= lift . f >>= State.put

spawnSection :: CitlisCoreLoop ()
spawnSection = do
  cd <- State.get
  case (+ negate 16) . coerce <$> Map.lookup SpawnTimer (view _timers cd) of
    Just msec
      | msec < 0 -> doSpawn
      | otherwise -> assign (_timers . ix SpawnTimer) (Millisec msec)
    Nothing -> pure ()
      
supplyNexts :: CoreData -> IO CoreData
supplyNexts coredata = 
  if length (view _nextMinos coredata) < 7
  then do
    nexts <- nextSeven
    pure $ over _nextMinos (<> nexts) coredata
  else pure coredata



consumeDelta :: Ord k => k -> Int -> Map k Millisec -> Map k Millisec
consumeDelta k delta = Map.adjust (over coerced (+ negate delta)) k


fCDtPC :: CoreData -> PortC -> PortC
fCDtPC cd =
  set (_gfxData . _holdV) hold .
  set (_gfxData . _nextMinos) (take 6 nexts) .
  set (_gfxData . _centerPaneState . _viewField) (takeViewField cd)
  where
    hold = preview _hold cd
    nexts = view _nextMinos cd

takeViewField :: CoreData -> ViewField
takeViewField cd = viewField
  where
    field = view _field cd
    mino = view _minoState cd
    viewField :: ViewField
    viewField =
      fieldToView $ fromMaybe field $ do
       (MinoState cm p _ _) <- mino
       m <- CList.focus cm
       let gp = ghostMinoPos m p field
       pure $ setMino m p $ setMino (shiftGhost m) gp field


gravitySection :: CitlisCoreLoop ()
gravitySection = do
  delta <- view (_difficulty . _gravity) <$> State.get
  modifying _chargedGravity (+ delta)
  rep . floor . view _chargedGravity =<< State.get
  modifying _chargedGravity (\grav -> grav - (floor grav % 1))
  where
    mino cd = CList.focus =<< preview (_minoState . _Just . _minoRing) cd
    pos = preview (_minoState . _Just . _pos)
    rep :: Int -> CitlisCoreLoop ()
    rep n
      | n < 1 = pure ()
      | otherwise = do
          cd <- State.get
          if fromMaybe False $ isLanding <$> mino cd <*> pos cd <*> preview _field cd
            then pure ()
            else do
            modifying (_minoState . _Just . _actionLog) (FallDown:)
            modifying (_minoState . _Just . _pos) (second (+1))
            rep (n-1)


applyLoop :: MinoAction -> MVar PortC -> CitlisCoreLoop ()
applyLoop act port = do
  r <- applyAction act port
  case r of
    Just a -> applyLoop a port
    Nothing -> pure ()

applyAction :: MinoAction -> MVar PortC -> CitlisCoreLoop (Maybe MinoAction)
applyAction act port = do
  cd <- State.get
  case act of
    SoftDrop -> do
      if fromMaybe False $ isLanding <$> mino cd <*> pos cd <*> field cd
        then pure $ Just Lock
        else do
          addLog
          modifying (_minoState . _Just . _pos) (second (+1))
          pure Nothing
    HardDrop -> do
      case ghostMinoPos <$> mino cd <*> pos cd <*> field cd of
        Just np -> do
          addLog
          assign (_minoState . _Just . _pos) np
          doLock port
          pure Nothing
        Nothing -> pure Nothing
    MoveRight -> do
      if fromMaybe False $ isValidPos <$> mino cd <*> (first (+1) <$> pos cd) <*> field cd
        then do
          addLog
          tickIgnoranceTimer
          modifying (_minoState . _Just . _pos) (first (+1))
          pure Nothing
        else
          pure Nothing
    MoveLeft -> do
      if fromMaybe False $ isValidPos <$> mino cd <*> (first (+ negate 1) <$> pos cd) <*> field cd
        then do
          addLog
          tickIgnoranceTimer
          modifying (_minoState . _Just . _pos) (first (+ negate 1))
          pure Nothing
        else
          pure Nothing
    RotateR -> do
      if fromMaybe False $ isValidPos <$> rmino cd <*> pos cd <*> field cd
        then do
          addLog
          tickIgnoranceTimer
          modifying (_minoState . _Just . _minoRing) rotateR
          pure Nothing
        else
          pure $ Just SuperRotateR
    RotateL -> do
      if fromMaybe False $ isValidPos <$> lmino cd <*> pos cd <*> field cd
        then do
          addLog
          tickIgnoranceTimer
          modifying (_minoState . _Just . _minoRing) rotateL
          pure Nothing
        else
          pure $ Just SuperRotateL
    SuperRotateR -> do
      case join $ superRotation <$> mino cd <*> rmino cd <*> pos cd <*> field cd of
        Just p -> do
          addLog
          tickIgnoranceTimer
          modifying (_minoState . _Just . _minoRing) rotateR
          assign (_minoState . _Just . _pos) p
          pure Nothing
        Nothing -> pure Nothing
    SuperRotateL -> do
      case join $ superRotation <$> mino cd <*> lmino cd <*> pos cd <*> field cd of
        Just p -> do
          addLog
          tickIgnoranceTimer
          modifying (_minoState . _Just . _minoRing) rotateL
          assign (_minoState . _Just . _pos) p
          pure Nothing
        Nothing -> pure Nothing
    Hold -> do
      case preview (_minoState . _Just) cd of
        Nothing -> pure Nothing
        Just mst ->
          if not (view _held mst)
          then do
            addLog
            case preview (_hold' . _Just) cd of
              Just cm -> do
                assign _hold' (resetRotation $ view _minoRing mst)
                assign _minoState (Just $ MinoState cm (initPosCL cm) mempty True)
              Nothing -> do
                assign _hold' (resetRotation $ view _minoRing mst)
                doSpawn
                assign (_minoState . _Just . _held) True
            pure Nothing
          else
            pure Nothing
    Lock -> doLock port >> pure Nothing
    _ -> pure Nothing
  where
    addLog = modifying (_minoState . _Just . _actionLog) (act:)
    tickIgnoranceTimer = modifying (_timers . ix LockTimer) (addDelta (genIgnoranceTimer def))
    mino0 = preview (_minoState . _Just . _minoRing)
    mino cd = CList.focus =<< mino0 cd
    pos = preview (_minoState . _Just . _pos)
    field = preview _field
    rmino cd = CList.focus =<< fmap rotateR (mino0 cd)
    lmino cd = CList.focus =<< fmap rotateL (mino0 cd)
    genIgnoranceTimer (Configure timers _) = fromMaybe (Millisec 0) $ Map.lookup LockIgnoranceDelay timers
    addDelta (Millisec a) (Millisec b) = Millisec (a+b)

