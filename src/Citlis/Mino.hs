
module Citlis.Mino where


import Prelude hiding (Either(..))
import qualified Data.List as List
import Data.List.NonEmpty (NonEmpty(..))
import Data.CircularList (CList)
import qualified Data.CircularList as CList
import Data.Key ((!))

import Citlis.Mino.Types


-- for testing
genDebugMino :: Char -> Maybe (CList Mino)
genDebugMino c = genRotations <$> List.find (\(Mino x _ _ _) -> c == x) minoset

{-# DEPRECATED dummyMino "shouldn't use it" #-}
dummyMino :: Mino
dummyMino = Mino '#' Stand (True :| []) 1

minoset :: [Mino]
minoset =
  [ Mino 'I' Stand
    (o :|
     [   o,o,o
     , x,x,x,x
     , o,o,o,o
     , o,o,o,o ]) 4
  , Mino 'J' Stand
    (x :|
     [   o,o
     , x,x,x
     , o,o,o ]) 3
  , Mino 'L' Stand
    (o :|
     [   o,x
     , x,x,x
     , o,o,o ]) 3
  , Mino 'O' Stand
    (x :|
     [   x
     , x,x ]) 2
  , Mino 'S' Stand
    (o :|
     [   x,x
     , x,x,o
     , o,o,o ]) 3
  , Mino 'Z' Stand
    (x :|
     [   x,o
     , o,x,x
     , o,o,o ]) 3
  , Mino 'T' Stand
    (o :|
     [   x,o
     , x,x,x
     , o,o,o ]) 3
  ]
  where
    o = False
    x = True

shiftGhost :: Mino -> Mino
shiftGhost (Mino _ r v n) = Mino '#' r v n

genRotations :: Mino -> CList Mino
genRotations (Mino c _ v n) =
  case n of
    4 ->
      CList.fromList
      [ Mino c Stand v n
      , Mino c Right  ( o :| [  o,x,o, o,o,x,o, o,o,x,o, o,o,x,o]) n
      , Mino c Invert ( o :| [  o,o,o, o,o,o,o, x,x,x,x, o,o,o,o]) n
      , Mino c Left   ( o :| [  x,o,o, o,x,o,o, o,x,o,o, o,x,o,o]) n
      ]
    3 ->
      CList.fromList
      [ Mino c Stand v n
      , Mino c Right  (rot v ( 6 :| [  3,0, 7,4,1, 8,5,2])) n
      , Mino c Invert (rot v ( 8 :| [  7,6, 5,4,3, 2,1,0])) n
      , Mino c Left   (rot v ( 2 :| [  5,8, 1,4,7, 0,3,6])) n
      ]
    _ ->
      CList.fromList $ (\a -> Mino c a v n) <$> [Stand, Right, Invert, Left]
  where
    o = False
    x = True
    rot l = fmap (l !) 

rotateR :: CList Mino -> CList Mino
rotateR = CList.rotR
rotateL :: CList Mino -> CList Mino
rotateL = CList.rotL


resetRotation :: CList Mino -> Maybe (CList Mino)
resetRotation = CList.findRotateTo (\(Mino _ s _ _) -> s == Stand)
