{-# LANGUAGE DeriveGeneric #-}

module Citlis.Types where

import Data.Default.Class
import Data.Ratio
import Data.Map (Map)
import qualified Data.Map as Map
import Data.CircularList (CList)
import qualified Data.CircularList as CList
import Control.Lens (Lens', Prism', Setter', Getter, Fold, _Just, folding)
import Control.Monad.Trans.State.Lazy (StateT)
import GHC.Generics (Generic)

import Citlis.Mino.Types (Mino(..))
import Citlis.View.Types (UserAction)



type CitlisCoreLoop = StateT CoreData IO
-- CitlisLoop CoreData a にする？

class HasHold a where
  _hold :: Fold a Mino
class HasNextMinos a where
  _nextMinos :: Lens' a [Mino]
class HasFrameCount a where
  _frameCount :: Lens' a FrameCount

newtype FrameCount = FrameCount Int
                   deriving (Show, Eq, Ord, Generic)
newtype Millisec = Millisec Int
                   deriving (Show, Eq, Ord, Generic)



data PortC = PortC GfxData SystemData
           deriving (Show, Eq, Ord)
data PortI = PortI InputData
           deriving (Show, Eq, Ord)
_gfxData :: Lens' PortC GfxData
_gfxData f (PortC a b) = flip PortC b <$> f a
_systemData :: Lens' PortC SystemData
_systemData f (PortC a b) = PortC a <$> f b
_inputData :: Lens' PortI InputData
_inputData f (PortI a) = PortI <$> f a
instance Default PortC where
  def = PortC def False


type PhenomenalIndex = Int
data GfxData = GfxData (Maybe Mino) [Mino] CenterPaneState [(Phenomenon, PhenomenalIndex)]
             deriving (Show, Eq, Ord)
instance HasHold GfxData where
  _hold = _holdV . _Just
_holdV :: Lens' GfxData (Maybe Mino)
_holdV f (GfxData a b c d) = (\x -> GfxData x b c d) <$> f a
instance HasNextMinos GfxData where
  _nextMinos f (GfxData a b c d) = (\x -> GfxData a x c d) <$> f b
_fieldState :: Lens' GfxData CenterPaneState
_fieldState f (GfxData a b c d) = (\x -> GfxData a b x d) <$> f c
_centerPaneState :: Lens' GfxData CenterPaneState
_centerPaneState = _fieldState
_latestPhenomena :: Lens' GfxData [(Phenomenon, PhenomenalIndex)]
_latestPhenomena f (GfxData a b c d) = (\x -> GfxData a b c x) . take 3 <$> f d
instance Default GfxData where
  def = GfxData Nothing mempty def mempty

type SystemData = Bool


type InputData = UserAction
type LeftPaneState = Maybe Mino

data ViewData = ViewData [Effect] PhenomenalIndex
              deriving (Show, Eq, Ord)
instance Default ViewData where
  def = ViewData mempty 0
_effects :: Lens' ViewData [Effect]
_effects f (ViewData a b) = flip ViewData b <$> f a
_phenomenalIndex :: Lens' ViewData PhenomenalIndex
_phenomenalIndex f (ViewData a b) = ViewData a <$> f b

data EraceType = ESingle | EDouble | ETriple | ETetris
               | ESpinMini
               | ESpinSingle
               | ESpinDouble
               | ESpinTriple
               deriving (Show, Eq, Ord)


type LineNumber = Int
type CellNumber = Int
data Phenomenon = PEmpty
                | PClearLine ViewField [LineNumber]
                | PLockMino ViewField [CellNumber]
                | PErasion EraceType Char
                | PGameOver ViewField
                deriving (Show, Eq, Ord)

data Effect = CommonEffect Phenomenon FrameCount
            deriving (Show, Eq, Ord)
instance HasFrameCount Effect where
  _frameCount f (CommonEffect a b) = CommonEffect a <$> f b
_phenomenon :: Getter Effect Phenomenon
_phenomenon f (CommonEffect a b) = flip CommonEffect b <$> f a


_cells' :: Fold Effect ViewField
_cells' = _phenomenon . _p
  where
    _p f (PClearLine a b) = flip PClearLine b <$> f a
    _p f (PLockMino a b) = flip PLockMino b <$> f a
    _p f (PGameOver a) = PGameOver <$> f a
    _p _ q = pure q

data CenterPaneState = CenterPaneState ViewField
                     deriving (Show, Eq, Ord)
_viewField :: Lens' CenterPaneState ViewField
_viewField f (CenterPaneState a) = CenterPaneState <$> f a
instance Default CenterPaneState where
  def = CenterPaneState def

type RightPaneState = [Mino]


type Pos = (Int,Int) -- x→ y↓


data MinoAction = Hold | Lock
                | MoveRight | MoveLeft
                | FallDown
                | SoftDrop | HardDrop
                | RotateR | RotateL
                | SuperRotateR | SuperRotateL
                deriving (Show, Eq, Ord)


data MinoState = MinoState (CList Mino) Pos [MinoAction] Bool
               deriving (Show, Eq)
initMinoState :: CList Mino -> Pos -> MinoState
initMinoState cl p = MinoState cl p mempty False
_minoRing :: Lens' MinoState (CList Mino)
_minoRing f (MinoState a b c d) = (\x -> MinoState x b c d) <$> f a
_pos :: Lens' MinoState Pos
_pos f (MinoState a b c d) = (\x -> MinoState a x c d) <$> f b
_actionLog :: Lens' MinoState [MinoAction]
_actionLog f (MinoState a b c d) = (\x -> MinoState a b x d) <$> f c
_held :: Lens' MinoState Bool
_held f (MinoState a b c d) = (\x -> MinoState a b c x) <$> f d
-- ^ False: remain hold. True: already held.

data EffectFrameIndex = ClearLineFrames
                      | LockMinoFrames
                      | ErasionShowFrames
                      | GameOverFrames
                      deriving (Show, Eq, Ord)

data TimerIndex = LockTimer
                | SpawnTimer
                | ErasionTimer
                deriving (Show, Eq, Ord)

data DelayIndex = LockIgnoranceDelay
                | SpawnDelay
                | LockDelay
                | ErasionDelay
                deriving (Show, Eq, Ord)

-- 1/64G 1/32G .... 3G 5G 20G
-- 64/60 1+ 1/15
type Gravity = Ratio Int
type GravityLine = [Gravity]
-- defGraivtyLine = [ 1 % 64, 1 % 32, 1 % 16, 1 % 8, 1 % 4, 1 % 1, 3 % 1, 5 % 1, 20 % 1]

data Configure = Configure (Map DelayIndex Millisec) (Map EffectFrameIndex FrameCount)
               deriving (Show, Eq)
instance Default Configure where
  def = Configure
        (Map.fromList
         [ (LockIgnoranceDelay, Millisec 12)
         , (LockDelay, Millisec 1500) -- 100 - 500
         , (SpawnDelay, Millisec 300) -- up to 500
         , (ErasionDelay, Millisec 700) -- 400 - 700
         ])
        (Map.fromList
         [ (ClearLineFrames, FrameCount 42) -- 700ms * 60f
         , (LockMinoFrames, FrameCount 18) -- 300ms * 60f (* 60 0.30)
         , (ErasionShowFrames, FrameCount 90)
         , (GameOverFrames, FrameCount 180)
         ])
_frameCountSetting :: Getter Configure (Map EffectFrameIndex FrameCount)
_frameCountSetting f (Configure a b) = Configure a <$> f b

-- Gravity が 1G 未満のときの 落下Δを保持する領域はどこ？
-- ここ TimerIndex だと 稼動timer だから DelayIndex にすべきな気がする
data Difficulty = Difficulty Gravity (Map TimerIndex Millisec)
                deriving (Show, Eq)
instance Default Difficulty where
  def = Difficulty (1 % 32) mempty
_gravity :: Lens' Difficulty Gravity
_gravity f (Difficulty a b) = (\x -> Difficulty x b) <$> f a

data GameOverData = GameOverData
                  deriving (Show, Eq)
-- CoreData を SceneBaseにして Lens' -> Prism' にしてあげたい
data CoreData = CoreData Field Gravity (Maybe MinoState) (Maybe (CList Mino)) [Mino]
                (Map TimerIndex Millisec) Difficulty
              deriving (Show, Eq)
instance Default CoreData where
  def = CoreData def (0 % 1) Nothing Nothing mempty mempty def
instance HasHold CoreData where
  _hold = _hold' . _Just . _cl
    where
      _cl :: Fold (CList a) a
      _cl = folding CList.focus
instance HasNextMinos CoreData where
  _nextMinos f (CoreData a b c d e h i) = (\xx -> CoreData a b c d xx h i) <$> f e
_hold' :: Lens' CoreData (Maybe (CList Mino))
_hold' f (CoreData a b c d e h i) = (\xx -> CoreData a b c xx e h i) <$> f d
_field :: Lens' CoreData Field
_field f (CoreData a b c d e h i) = (\xx -> CoreData xx b c d e h i) <$> f a
_minoState :: Lens' CoreData (Maybe MinoState)
_minoState f (CoreData a b c d e h i) = (\xx -> CoreData a b xx d e h i) <$> f c
_timers :: Lens' CoreData (Map TimerIndex Millisec)
_timers f (CoreData a b c d e h i) = (\xx -> CoreData a b c d e xx i) <$> f h
_chargedGravity :: Lens' CoreData Gravity
_chargedGravity f (CoreData a b c d e h i) = (\xx -> CoreData a xx c d e h i) <$> f b
_difficulty :: Lens' CoreData Difficulty
_difficulty f (CoreData a b c d e h i) = (\xx -> CoreData a b c d e h xx) <$> f i

class FieldLike a where
  _cells :: Lens' a [Char]
  _fieldWidth :: Lens' a Int

data Field = Field [Char] Int deriving (Show, Eq, Ord)
data ViewField = ViewField [Char] Int deriving (Show, Eq, Ord)
instance Default Field where
  def = Field (replicate 220 ' ') 10
instance Default ViewField where
  def = ViewField (replicate 200 ' ') 10
instance FieldLike Field where
  _cells f (Field a b) = flip Field b <$> f a
  _fieldWidth f (Field a b) = Field a <$> f b
instance FieldLike ViewField where
  _cells f (ViewField a b) = flip ViewField b <$> f a
  _fieldWidth f (ViewField a b) = ViewField a <$> f b


