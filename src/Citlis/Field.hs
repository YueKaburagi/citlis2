{-# LANGUAGE TupleSections #-}

module Citlis.Field where

import Prelude hiding (and, zip)

import Data.Semigroup ((<>))
import Data.Key ((!), zip, replace, mapWithKey)
import Data.Maybe (catMaybes, maybe)
import Data.Bifunctor (bimap)
import Data.Bool (bool)
import Data.Foldable (toList)
import Data.Function ((&))
import Data.Bifunctor (first, second)
import Data.CircularList (CList)
import qualified Data.CircularList as CList
import Data.Random (shuffle, runRVar, StdRandom(StdRandom))
import Data.Default.Class
import Control.Applicative ((<|>))
import Control.Lens (Lens')

import Citlis.Types
import Citlis.Mino
import Citlis.Mino.Types (Mino(..))
import qualified Citlis.Mino.Types as Mino



fieldToView :: Field -> ViewField
fieldToView (Field v n) = ViewField (drop (2*n) v) n


clearLine :: Field -> (Field, [Int]) -- 20行のうちどこかを返す
clearLine (Field v n) = settle $ rep 0 otherLines
  where
    head2Lines = take (2*n) v
    otherLines = drop (2*n) v
    settle p@(_,y) = first (flip Field n . (replicate (n* length y) ' ' <>) . (head2Lines <>) ) p
    aline :: Int -> [Char] -> Either [Char] Int
    aline i l = bool (Left l) (Right i) $ notElem ' ' l
    rep :: Int -> [Char] -> ([Char], [Int])
    rep _ [] = ([], [])
    rep i l =
      rep (i+1) (drop n l) &
      case aline i (take n l) of
        Left t -> first (t <>)
        Right t -> second (t:)

isValidPos :: Mino -> Pos -> Field -> Bool
isValidPos (Mino _ _ v q) pos field = all and $ zip view $ toList v
  where
    view = viewSquare field pos q
    and (' ',_) = True
    and (_,False) = True
    and _ = False

setMino :: Mino -> Pos -> Field -> Field
setMino (Mino ch _ v q) (x,y) (Field xs n) =
  Field (take (n*y) xs <> rep (bool ' ' ch <$> toList v) (drop (n*y) xs)) n
  where
    aline :: Int -> [Char] -> [Char] -> [Char]
    aline _ [] = id
    aline i (' ':cs) = aline (i+1) cs
    aline i (c:cs) = aline (i+1) cs . replace i c
    rep [] l = l
    rep r l = aline x (take q r) (take n l) <> rep (drop q r) (drop n l)

viewSquare :: Field -> Pos -> Int -> [Char]
viewSquare (Field v n) (x,y) q = rep takeLines
  where
    takeLines = take (n*q) (drop (y*n) v <> repeat '#')
    aline l | x < 0 = take q (replicate (abs x) '#' <> l)
            | otherwise = take q $ drop x (l <> repeat '#')
    rep [] = []
    rep l = aline (take n l) <> rep (drop n l)

spawnMino :: [Mino] -> (CList Mino, Pos, [Mino])
spawnMino nexts = (genRotations h, initPos h, tail nexts)
  where
    h = head nexts
lookAheadMino :: [Mino] -> (CList Mino, Pos)
lookAheadMino = onetwo . spawnMino
  where
    onetwo (a,b,_) = (a,b)

initPosCL :: CList Mino -> Pos
initPosCL = maybe (0,0) initPos . CList.focus
initPos :: Mino -> Pos
initPos (Mino _ _ _ q) =
  case q of
    4 -> (3,0)
    3 -> (3,0)
    2 -> (4,0)
    _ -> (0,0)

ghostMinoPos :: Mino -> Pos -> Field -> Pos
ghostMinoPos mino pos@(x,y) field =
  if isValidPos mino next field
  then ghostMinoPos mino next field
  else pos
  where
    next = (x,y+1)

isLanding :: Mino -> Pos -> Field -> Bool
isLanding mino (x,y) = not . isValidPos mino (x,y+1)


minoToCellNums :: Mino -> Pos -> Field -> [Int]
minoToCellNums (Mino _ _ bs q) (x,y) (Field _ n) = catMaybes $ aline start $ toList bs
  where
    start = n*y + x
    count i True = Just i
    count _ _ = Nothing
    aline :: Int -> [Bool] -> [Maybe Int]
    aline _ [] = []
    aline p l = (fmap (+p) <$> mapWithKey count (take q l)) <> aline (p+n) (drop q l)



superRotation :: Mino -> Mino -> Pos -> Field -> Maybe Pos
superRotation m0 mz (x,y) z =
  gen $ reverse matrix
  where
    addPos (a,b) = (a+x,b+y)    
    gen = foldr (\o s -> s <|> bool Nothing (Just (addPos o)) (isValidPos mz (addPos o) z)) Nothing
    matrix =
      case (m0,mz) of
        (Mino _ _ _ 3, Mino _ Mino.Right _ 3) ->
          [(-1, 0), (-1,-1), ( 0, 2), (-1, 2)]
        (Mino _ Mino.Left _ 3, Mino _ _ _ 3) ->
          [(-1, 0), (-1, 1), ( 0,-2), (-1,-2)]
        (Mino _ Mino.Right _ 3, Mino _ _ _ 3) ->
          [( 1, 0), ( 1, 1), ( 0,-2), ( 1,-2)]
        (Mino _ _ _ 3, Mino _ Mino.Left _ 3) ->
          [( 1, 0), ( 1,-1), ( 0, 2), ( 1, 2)]
        (Mino _ Mino.Stand _ 4, Mino _ Mino.Right _ 4) ->
          [(-2, 0), ( 1, 0), (-2, 1), ( 1,-2)]
        (Mino _ Mino.Left _ 4, Mino _ Mino.Invert _ 4) ->
          [(-2, 0), ( 1, 0), (-2, 1), ( 1,-2)]
        (Mino _ Mino.Right _ 4, Mino _ Mino.Stand _ 4) ->
          [( 2, 0), (-1, 0), ( 2,-1), (-1, 2)]
        (Mino _ Mino.Invert _ 4, Mino _ Mino.Left _ 4) ->
          [( 2, 0), (-1, 0), ( 2,-1), (-1, 2)]
        (Mino _ Mino.Right _ 4, Mino _ Mino.Invert _ 4) ->
          [(-1, 0), ( 2, 0), (-1,-2), ( 2, 1)]
        (Mino _ Mino.Stand _ 4, Mino _ Mino.Left _ 4) ->
          [(-1, 0), ( 2, 0), (-1,-2), ( 2, 1)]
        (Mino _ Mino.Invert _ 4, Mino _ Mino.Right _ 4) ->
          [( 1, 0), (-2, 0), ( 1, 2), (-2,-1)]
        (Mino _ Mino.Left _ 4, Mino _ Mino.Stand _ 4) ->
          [( 1, 0), (-2, 0), ( 1, 2), (-2,-1)]
        _ -> []




isSpinInLocation :: Mino -> Pos -> Field -> Bool
isSpinInLocation (Mino c r _ _) pos field =
  validate pLines
  where
    add (x,y) = bimap (+x) (+y) pos
    validate (n,l) = n <= length (filter id $ fmap (any (flip isFilled field . add)) l)
    pLines =
      case c of
        'I' -> (,) 2 $
          case r of
            Mino.Stand  -> [(,0) <$> [0..3], (,2) <$> [0..3]]
            Mino.Right  -> [(1,) <$> [0..3], (3,) <$> [0..3]]
            Mino.Left   -> [(0,) <$> [0..3], (2,) <$> [0..3]]
            Mino.Invert -> [(,1) <$> [0..3], (,3) <$> [0..3]]
        'S' -> (,) 2 $
          case r of
            Mino.Stand  -> [[(0,0), (2,-1)], [(0,2), (2,1)]]
            Mino.Right  -> [[(0,0), (1,2)], [(2,0), (3,2)]]
            Mino.Left   -> [[(-1,0), (0,2)], [(1,0), (2,2)]]
            Mino.Invert -> [[(0,1), (2,0)], [(0,3), (2,2)]]
        'Z' -> (,) 2 $
          case r of
            Mino.Stand  -> [[(0,-1), (2,0)], [(1,0), (2,2)]]
            Mino.Right  -> [[(0,2), (1,0)], [(2,2), (3,0)]]
            Mino.Left   -> [[(-1,2), (0,0)], [(1,2), (2,0)]]
            Mino.Invert -> [[(0,0), (2,1)], [(0,3), (2,2)]]
        'J' -> (,) 2 $
          case r of
            Mino.Stand  -> [(,0) <$> [1..2], (,2) <$> [0..2]]
            Mino.Right  -> [(0,) <$> [0..2], (2,) <$> [1..2]]
            Mino.Left   -> [(0,) <$> [0..1], (2,) <$> [0..2]]
            Mino.Invert -> [(,0) <$> [0..2], (,2) <$> [0..1]]
        'L' -> (,) 2 $
          case r of
            Mino.Stand  -> [(,0) <$> [0..1], (,2) <$> [0..2]]
            Mino.Right  -> [(0,) <$> [0..2], (2,) <$> [0..1]]
            Mino.Left   -> [(0,) <$> [1..2], (2,) <$> [0..2]]
            Mino.Invert -> [(,0) <$> [0..2], (,2) <$> [1..2]]
        'T' -> (,) 3 [[(0,0)], [(2,0)], [(2,2)], [(0,2)]]
        _ -> (1,[])

isFilled :: Pos -> Field -> Bool
isFilled (x,y) (Field cs n) = x < 0 || x >= n || y < 0 || y >= 22 || ' ' /= (cs ! (y*n +x))
