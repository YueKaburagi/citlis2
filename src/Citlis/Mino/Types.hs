
module Citlis.Mino.Types where

import Data.List.NonEmpty (NonEmpty)

data Rotation = Stand | Left | Invert | Right
              deriving (Show, Eq, Ord)

data Mino = Mino Char Rotation (NonEmpty Bool) Int
          deriving (Show, Eq, Ord)


