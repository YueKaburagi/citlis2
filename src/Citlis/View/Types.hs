
module Citlis.View.Types where

data UserAction = RotateR | RotateL | Hold
                | MoveR | MoveL | SoftDrop | HardDrop
                | Quit
                deriving (Show, Eq, Ord)

