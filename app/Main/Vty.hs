{-# LANGUAGE LambdaCase #-}

module Main.Vty where

import Prelude hiding (zip, pi, lines)

import Data.Default.Class (def)
import Data.Foldable (toList, traverse_)
import Data.Bool (bool)
import Data.List (intercalate)
import Data.Coerce (coerce)
import Data.Bifunctor (first, second)
import Data.Maybe (fromMaybe)
import Data.Key
import qualified Data.Map as Map
import Graphics.Vty hiding ((<|>))
import qualified Graphics.Vty.Attributes as Color
import Control.Concurrent (readMVar, tryPutMVar, MVar, threadDelay)
import Control.Monad (void)
import Control.Lens (Lens', view, preview, over, ix, assign, modifying, coerced)
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.State.Lazy (StateT)
import qualified Control.Monad.Trans.State.Lazy as State
import Control.Applicative ((<|>))

import Citlis.Types
import qualified Citlis.View.Types as V
import Citlis.Mino.Types

import Main.Vty.Mino

type ViewLoop = StateT ViewData IO

vtyMain :: MVar PortC -> MVar PortI -> IO ()
vtyMain portC portI = do
  cfg <- standardIOConfig
  vty <- mkVty cfg
  State.evalStateT (loop vty portC portI) def
  shutdown vty
  where
    loop :: Vty -> MVar PortC -> MVar PortI -> ViewLoop ()
    loop vty pc pi = do
      updateVD pc
      gfxSection vty pc
      lift $ inputSection vty pi pc
      -- gameoverで終了したいので姑息な対応
      effs <- view _effects <$> State.get
      if any (\case
                 CommonEffect (PGameOver _) _ -> True
                 _ -> False
          ) effs
        then void $ lift $ tryPutMVar portI (PortI V.Quit)
        else pure ()
      -- 姑息区間ここまで
      st <- lift $ readMVar pc
      case view _systemData st of
        True -> pure ()
        False -> lift (threadDelay 16666) >> loop vty pc pi

updateVD :: MVar PortC -> ViewLoop ()
updateVD port = do
  countdown
  removeDeads
  l <- lift $ view (_gfxData . _latestPhenomena) <$> readMVar port
  curi <- view _phenomenalIndex <$> State.get
  traverse_ (uncurry (raisePhenomenon curi)) l
  where
    countdown = modifying _effects (fmap (over (_frameCount . coerced) (+ negate (1 :: Int))))
    removeDeads = modifying _effects (filter ((> (0 :: Int)) . view (_frameCount . coerced))) 

raisePhenomenon :: PhenomenalIndex -> Phenomenon -> PhenomenalIndex -> ViewLoop ()
raisePhenomenon curi phn i
  | i > curi = do
    case phn of
      PClearLine _ _ ->
        modifying _effects (CommonEffect phn (getTime ClearLineFrames def) :)
      PLockMino _ _ ->
        modifying _effects (CommonEffect phn (getTime LockMinoFrames def) :)
      PErasion _ _ ->
        modifying _effects (CommonEffect phn (getTime ErasionShowFrames def) :)
      PGameOver _ ->
        modifying _effects (CommonEffect phn (getTime GameOverFrames def) :)
      _ -> pure ()
    assign _phenomenalIndex i
  | otherwise = pure ()
  where
    getTime k = fromMaybe (FrameCount 0) . Map.lookup k . view _frameCountSetting



gfxSection :: Vty -> MVar PortC -> ViewLoop ()
gfxSection vty port = do
  st <- lift $ readMVar port
  lp <- drawLeftPane (preview (_gfxData . _hold) st)
  cp <- drawCenterPane (view (_gfxData . _fieldState) st)
  lift $ update vty $ picForImage $ horizCat
    [ lp, cp
    , drawRightPane (view (_gfxData . _nextMinos) st)
    ]

inputSection :: Vty -> MVar PortI -> MVar PortC -> IO ()
inputSection vty port _ = do
  e <- nextEventNonblocking vty
  maybe (pure ()) (
    \case
      EvKey KEsc _ -> void $ tryPutMVar port (PortI V.Quit)
      EvKey KUp _ -> void $ tryPutMVar port (PortI V.HardDrop)
      EvKey KDown _ -> void $ tryPutMVar port (PortI V.SoftDrop)
      EvKey KLeft _ -> void $ tryPutMVar port (PortI V.MoveL)
      EvKey KRight _ -> void $ tryPutMVar port (PortI V.MoveR)
      EvKey KEnter _ -> void $ tryPutMVar port (PortI V.Hold)
      EvKey (KChar 'z') _ -> void $ tryPutMVar port (PortI V.RotateL)
      EvKey (KChar 'x') _ -> void $ tryPutMVar port (PortI V.RotateR)
      _ -> pure ()
    ) e

padString :: Int -> Int -> String -> String
padString l r s = replicate l ' ' <> s <> replicate r ' '
padImage :: Int -> Int -> Image -> Image
padImage l r s = string defAttr (replicate l ' ') `horizJoin` s `horizJoin` string defAttr (replicate r ' ')

drawMino2L :: Mino -> [Image] -- w 4 h 2
drawMino2L (Mino c _ v n) =
  case n of
    3 ->
      [aline (padString 0 1 $ take n ls), aline (padString 0 1 $ take n $ drop n ls)]
    2 ->
      [aline (padString 1 1 $ take n ls), aline (padString 1 1 $ take n $ drop n ls)]
    _ ->
      [aline (take n ls), aline (take n $ drop n ls)]
  where
    aline = string (defAttr `withForeColor` cellColor c)
    ls = toList $ toc <$> v
    toc = bool ' ' '▇'

drawHold :: Maybe Mino -> [Image] -- w 8 h 4
drawHold mino =
  decolate $ maybe (replicate 2 $ string defAttr "    ") drawMino2L mino
  where
    aline = string (defAttr `withForeColor` cellColor '!')
    decolate ims = [aline "+-Hold-+"] <>
                    fmap (\s -> aline "| " `horizJoin` s `horizJoin` aline " |") ims <>
                    [aline "+------+"]

drawLeftPane :: LeftPaneState -> ViewLoop Image -- w 15 h 21
drawLeftPane st = do
  effs <- view _effects <$> State.get
  pure $ vertCat
    ( replicate 1 emptyLine <>
      fit 4 3 (drawHold st) <>
      replicate 4 emptyLine <>
      [genErasionLine effs] <>
      replicate 11 emptyLine
    )
  where
    emptyLine = string defAttr $ replicate 15 ' '
    fit l r = fmap (padImage l r)
    genErasionLine = fromMaybe emptyLine . foldr (\a s -> s <|> pEv a) Nothing
    pEv (CommonEffect (PErasion a b) _) = Just $ pErase a b
    pEv _ = Nothing
    pErase et c =
      case et of
        ESingle -> string (defAttr `withForeColor` cellColor c) "    single     "
        EDouble -> string (defAttr `withForeColor` cellColor c) "  - Double -   "
        ETriple -> string (defAttr `withForeColor` cellColor c) "  + Triple +   "
        ETetris -> string (defAttr `withForeColor` cellColor c) " ** TETRIS **  "
        ESpinMini ->
          horizCat
          [ string defAttr "  "
          , char (defAttr `withForeColor` cellColor c) c
          , char defAttr '-'
          , string (defAttr `withForeColor` cellColor c) "Spin"
          , string defAttr " mini  " ]
        ESpinSingle ->
          horizCat
          [ string defAttr " "
          , char (defAttr `withForeColor` cellColor c) c
          , char defAttr '-'
          , string (defAttr `withForeColor` cellColor c) "Spin"
          , string defAttr " single " ]
        ESpinDouble ->
          horizCat
          [ string defAttr " "
          , char (defAttr `withForeColor` cellColor c) c
          , char defAttr '-'
          , string (defAttr `withForeColor` cellColor c) "Spin"
          , string defAttr " Double " ]
        ESpinTriple ->
          horizCat
          [ string defAttr " "
          , char (defAttr `withForeColor` cellColor c) c
          , char defAttr '-'
          , string (defAttr `withForeColor` cellColor c) "Spin"
          , string defAttr " Triple " ]


drawCenterPane :: CenterPaneState -> ViewLoop Image -- w 12 h 21
drawCenterPane (CenterPaneState field) = do
  effs <- view _effects <$> State.get
  let fld = fromMaybe field $ foldr (\a s -> s <|> preview _cells' a) Nothing effs
  let num = view _fieldWidth fld
  let imgs = unloadACFat $ foldr (\x s -> applyFieldEffect x num s) (raiseAC fld) effs
  pure $ vertCat $ decolate $ chunkImage num imgs
  where
    aline = string defAttr
    chunkImage _ [] = []
    chunkImage n l = horizCat (take n l) : chunkImage n (drop n l)
    decolate imgs = fmap (\s -> aline "|" `horizJoin` s `horizJoin` aline "|") imgs <>
                    [aline ("+" <> replicate 18 '-' <> "+")]

-- data ViewLike a = ViewLike Int [a]
raiseAC :: ViewField -> [(Attr, Char)]
raiseAC = fmap toAC . view _cells
  where
    toAC c = (defAttr `withForeColor` cellColor c `withBackColor` cellBG c, bool ' ' '▇' $ c /= ' ')

applyFieldEffect :: Effect -> Int -> [(Attr, Char)] -> [(Attr, Char)]
applyFieldEffect eff num vs =
  case view _phenomenon eff of
    PLockMino _ cells ->
      mapWithKey (chrF id (first (`withBackColor` Color.white)) cells) vs
    PClearLine _ lines ->
      concat $ li lines <$> sliceWithIndex 0 vs
    PGameOver _ ->
      (first (`withForeColor` Color.red)) <$> vs
    _ -> vs
  where
    chrF f g l n = bool f g $ n `elem` l
    sliceWithIndex _ [] = []
    sliceWithIndex i v = (i, take num v) : sliceWithIndex (i+1) (drop num v)
    merge :: [Char] -> [(Attr, Char)] -> [(Attr, Char)]
    merge l r = (\(a,(b,c)) -> bool (b,c) (defAttr,a) $ a /= ' ') <$> zip l r
    li :: [Int] -> (Int, [(Attr, Char)]) -> [(Attr,Char)]
    li l (n,v) | n `elem` l = merge (take num $ genEraseEffect (view _frameCount eff)) v
               | otherwise = v

unloadAC :: [(Attr, Char)] -> [Image]
unloadAC = fmap (uncurry char)
unloadACFat :: [(Attr, Char)] -> [Image]
unloadACFat = fmap (uncurry string . second rep)
  where
    rep '▇' = "█▉"
    rep ' ' = "  "
    rep '=' = "=="
    rep '>' = ">>"
    rep _   = "__"

-- 元々のcellの文字色を背景色にすると見栄えよさそう
genEraseEffect :: FrameCount -> [Char]
genEraseEffect (FrameCount c) =
  drop (min 12 (c `div` 3)) "==========>>          "

mergeLine :: [Char] -> [Char] -> [Char]
mergeLine a b = (\(x,y) -> bool y x $ x == ' ' ) <$> zip a b



drawNexts :: [Mino] -> [Image] -- w 8 h 10
drawNexts minos = decolate $ intercalate [emptyBL] $ drawMino2L <$> minos
  where
    emptyBL = string defAttr $ replicate 4 ' '
    aline = string (defAttr `withForeColor` cellColor '!')
    decolate ims = [aline "+-Next-+"] <>
                    fmap (\s -> aline "| " `horizJoin` s `horizJoin` aline " |") ims <>
                    [aline "+------+"]


drawRightPane :: RightPaneState -> Image
drawRightPane st =
  vertCat $ fit 1 1 $ drawNexts (take 3 st)
  where
    fit l r = fmap (padImage l r)
