
module Main.Vty.Mino where



import Prelude hiding (Either(..))
import Data.List.NonEmpty (NonEmpty(..))
import Data.CircularList (CList)
import qualified Data.CircularList as CList
import Data.Key ((!))

import Citlis.Mino.Types


import Graphics.Vty.Attributes (Color)
import qualified Graphics.Vty.Attributes as Color

cellColor :: Char -> Color
cellColor 'I' = Color.brightCyan
cellColor 'O' = Color.brightYellow
cellColor 'S' = Color.brightGreen
cellColor 'Z' = Color.brightRed
cellColor 'J' = Color.brightBlue
cellColor 'L' = Color.yellow
cellColor 'T' = Color.magenta
cellColor '#' = Color.brightBlack
cellColor '=' = Color.black
cellColor '>' = Color.black
cellColor 'x' = Color.cyan
cellColor _ = Color.white
-- ghost color  = Color.brightBlack

cellBG :: Char -> Color
cellBG '=' = Color.brightWhite
cellBG '>' = Color.brightWhite
cellBG _ = Color.black

