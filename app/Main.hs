module Main where


import Prelude hiding (and, zip, log)

import Data.Semigroup ((<>))
import Data.Key (zip, replace)
import Data.Bool (bool)
import Data.Coerce (coerce)
import Data.Maybe (fromMaybe)
import qualified Data.List as List
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Foldable (toList)
import Data.Function ((&))
import Data.Bifunctor (first, second, bimap)
import Data.CircularList (CList)
import qualified Data.CircularList as CList
import Data.Random (shuffle, runRVar, StdRandom(StdRandom))
import Data.Default.Class
import Control.Concurrent (newMVar, newEmptyMVar, forkIO, killThread, MVar, readMVar, tryTakeMVar, modifyMVar_, threadDelay, yield)
import Control.Lens (view, preview, set, over, _Just, coerced, modifying, assign, ix)
import Control.Monad (void, join)
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.State.Lazy (StateT)
import qualified Control.Monad.Trans.State.Lazy as State

import Citlis.Core
import Citlis.Types
import Citlis.View.Types (UserAction)
import qualified Citlis.View.Types as V
import Citlis.Field
import Citlis.Mino (minoset, dummyMino, rotateR, rotateL, resetRotation, shiftGhost)
import Citlis.Mino.Types (Mino(..))
import qualified Citlis.Mino.Types as Mino
import Main.Vty (vtyMain)

import Debug.Trace

data Mode = StandardMode | NoVtyMode
          deriving (Show, Eq, Ord)

mode = StandardMode
--mode = NoVtyMode



-- no Vty mode?
main :: IO ()
main = do
  nexts <- nextSeven
  let cd = set _timers (Map.singleton SpawnTimer (Millisec 600)) $ set _nextMinos nexts def
  portC <- newMVar def
  portI <- newEmptyMVar
  modifyMVar_ portC (pure . fCDtPC cd)
  case mode of
    StandardMode -> do
      vux <- forkIO $ vtyMain portC portI
      _ <- State.execStateT (coreLoop portC portI) cd
      threadDelay 32000 -- 本当はvuxが終わるまで待ちたい VQuitの後にVVanishedを置いてそっちを終了条件にする？
      yield
      killThread vux
    NoVtyMode ->
      void $ State.execStateT (coreLoop portC portI) cd
    
