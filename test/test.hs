
import Test.Tasty
import Test.Tasty.QuickCheck as QC
import Test.Tasty.Hspec
import Test.Tasty.HUnit
import Test.Hspec

import Control.Lens (set, preview, ix)
import Data.Default.Class (def)
import Control.Concurrent (MVar, newMVar, newEmptyMVar, readMVar, tryPutMVar)
import Data.Functor (void)
import qualified Data.CircularList as CList
import Control.Monad.Trans.State.Lazy (execStateT)

import Citlis.Core (coreOneStep, nextSeven)
import Citlis.Types
import Citlis.View.Types (UserAction)
import qualified Citlis.View.Types as V
import Citlis.Field ()
import Citlis.Mino (genDebugMino, rotateR, rotateL)
import Citlis.Mino.Types (Mino(..))
import qualified Citlis.Mino.Types as Mino

main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests" [superRots]


superRots :: TestTree
superRots =
  testGroup "SuperRoration"
  [ superRotsUnit "T R->u spin-double" (PortI V.RotateR) (
      setField (
          "xxxx  xxxx" <>
          "xxx   xxxx" <>
          "xxxx xxxxx"
          ) $ setMino 'T' 1 (3,2) def
      ) (genRsP $ PErasion ESpinDouble 'T')
  , superRotsUnit "T L->u spin-mini" (PortI V.RotateL) (
      setField (
          "xxxx  xxxx" <>
          "xxxx  xxxx" <>
          "xxx   xxxx" <>
          "xxx  xxxxx"
          ) $ setMino 'T' (-1) (4,3) def
      ) (genRsP $ PErasion ESpinMini 'T')
  , superRotsUnit "T L->u spin-double" (PortI V.RotateL) (
      setField (
          "xxx  xxxxx" <>
          "xxx   xxxx" <>
          "xxxx xxxxx"
          ) $ setMino 'T' (-1) (3,2) def
      ) (genRsP $ PErasion ESpinDouble 'T')
  , superRotsUnit "S s->R spin-triple" (PortI V.RotateR) (
      setField (
          "   xxx    " <>
          "   x      " <>
          "          " <>
          "xxx xxxxxx" <>
          "xxx  xxxxx" <>
          "xxxx xxxxx"
          ) $ setMino 'S' 0 (3,4) def
      ) (genRsP $ PErasion ESpinTriple 'S')
  , superRotsUnit "S s->R spin-double" (PortI V.RotateR) (
      setField (
          "   xxx    " <>
          "   x      " <>
          "          " <>
          "x    xxxxx" <>
          "xxx  xxxxx" <>
          "xxxx xxxxx"
          ) $ setMino 'S' 0 (3,4) def
      ) (genRsP $ PErasion ESpinDouble 'S')
  , superRotsUnit "S s->R spin-mini" (PortI V.RotateR) (
      setField (
          "   xxx    " <>
          "   x      " <>
          "          " <>
          "xx   xxxxx" <>
          "xxx  xxxxx" <>
          "xxx   xxxx"
          ) $ setMino 'S' 0 (3,4) def
      ) (genRsP $ PErasion ESpinMini 'S')
  ]

superRotsUnit :: TestName -> PortI -> CoreData -> (PortC -> Bool) -> TestTree
superRotsUnit name pid cdd rsc =
  testCase name $ do
    nexts <- nextSeven
    portC <- newMVar def
    portI <- newEmptyMVar
    setPortI portI pid
    d1 <- execStateT (coreOneStep portC portI) (set _nextMinos nexts cdd)
    setPortI portI (PortI V.SoftDrop)
    void $ execStateT (coreOneStep portC portI) d1
    vc <- readMVar portC
    vc `shouldSatisfy` rsc



setPortI :: MVar PortI -> PortI -> IO ()
setPortI port = void . tryPutMVar port

genRsP :: Phenomenon -> PortC -> Bool
genRsP p port = (== Just p) $
  fst <$> preview (_gfxData . _latestPhenomena . ix 0) port

invertY :: Pos -> Pos
invertY (x,y) = (x,21 - y)

setMino :: Char -> Int -> Pos -> CoreData -> CoreData
setMino c r p =
  set _minoState $ initMinoState <$> (CList.rotN r <$> genDebugMino c) <*> pure (invertY p)

setField :: String -> CoreData -> CoreData
setField part = set (_field . _cells) field
  where
    field = replicate (220 - length part) ' ' <> part
